# modèle et gabarit de document

En cours de test

les formats :   
  - .odt : document texte
  - .ott : modèle format ouvert (OpenOffice ...)
  - .dot : modèle pour word (si le format .ott ne passe pas)
  - .pdf : pour contrôler visuellement que votre éditeur prenne bien en charge le modèle

 Logiciel utile : 
  - https://www.openoffice.org/fr/
